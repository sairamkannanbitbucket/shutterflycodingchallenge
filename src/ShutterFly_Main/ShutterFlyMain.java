package ShutterFly_Main;
import java.io.*;
import com.google.gson.*;
import LTV_Calc.LTV_Calculator;
import Shutter_Ingest.Shutter_IO_Ingester;
public class ShutterFlyMain { 
	public static void main(String[] args)
	{
	try(BufferedReader br = new BufferedReader(new FileReader("./input/input1.txt"))) {
	    StringBuilder sb = new StringBuilder();
	    String line = br.readLine();
	    while (line != null) {
	        sb.append(line);
	        sb.append(System.lineSeparator());
	        line = br.readLine();
	    }	    
	    String everything = sb.toString();	   
	    JsonArray jsonArray = (JsonArray)(new JsonParser().parse(everything));
        for(JsonElement e: jsonArray){
	      	System.out.println(e.toString());
	    	Shutter_IO_Ingester IOingest = new Shutter_IO_Ingester();
	    	IOingest.ingest(e.toString()); 
	    }	    
	    LTV_Calculator LTVcalc = new LTV_Calculator();
	    LTVcalc.TopXSimpleLTVCustomers(3); 
	    }
	catch (FileNotFoundException e) {
		System.out.println("file exp::"+e);
	}
	catch (IOException e) {
		System.out.println("io exp::"+e);
	}	
}
}
